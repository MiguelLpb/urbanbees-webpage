 var firebaseConfig = {
    apiKey: "AIzaSyCQ_YZiI9-83fT8vRgd-t5TDyBdx4KMy6c",
    authDomain: "urbanbees-e9017.firebaseapp.com",
    databaseURL: "https://urbanbees-e9017.firebaseio.com",
    projectId: "urbanbees-e9017",
    storageBucket: "urbanbees-e9017.appspot.com",
    messagingSenderId: "678903835124",
    appId: "1:678903835124:web:47e050e06a6c45ff"
  };
  firebase.initializeApp(firebaseConfig);

 


function adminpage(){

    firebase.auth().signInWithEmailAndPassword($(".adminname").val(), $(".adminpassword").val()).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // ...
  alert("error: "+ errorMessage);
  });
  
}
function logout (){
    firebase.auth().signOut().then(function(){
        window.location= "./admin.html";
    }).catch(function(error){
        
    });
} 
 $(document).ready(function() {  
        // Your web app's Firebase configuration
   firebase.auth().onAuthStateChanged(function(user){
     
    if (user){
        var user = firebase.auth().currentUser; 
       if(user !=null &&   user.uid=="i9oSncyZDyRSXbE1gLds43KMCSs1"){ 
           if(window.location.href.indexOf("admin.html") !=-1){
            window.location= "./admindata.html";
           }
      }
    }  else{
        if(window.location.href.indexOf("admin.html") ==-1){
            window.location= "./admin.html";
        }
    }
  }); 
  
   
  
  
 if(document.body.contains(document.getElementById('eventchart'))){ 
     
     
  
  var dataevents=[0,0,0,0,0,0];
  //0: hackathon, 1:volunteer ,2: conference,3: workshop,4: festival,5:holiday
  // Initialize Firebase
  
var messageRef= firebase.database().ref("/Cities/Hamburg/Events").once("value", function(snapshot) {
  snapshot.forEach(function(childSnapshot) {
      if(childSnapshot.child("type").val()=="hackathon"){
          var dataeventsvalue=dataevents[0]+1;
          dataevents[0]=dataeventsvalue;            
      }else if(childSnapshot.child("type").val()=="volunteer"){
            var dataeventsvalue=dataevents[1]+1;
          dataevents[1]=dataeventsvalue;
      }else if(childSnapshot.child("type").val()=="conference"){
            var dataeventsvalue=dataevents[2]+1;
          dataevents[2]=dataeventsvalue;
      }else if(childSnapshot.child("type").val()=="workshop"){
            var dataeventsvalue=dataevents[3]+1;
          dataevents[3]=dataeventsvalue;
      }else if(childSnapshot.child("type").val()=="festival"){
            var dataeventsvalue=dataevents[4]+1;
          dataevents[4]=dataeventsvalue;
      }else if(childSnapshot.child("type").val()=="holiday"){
           var dataeventsvalue=dataevents[5]+1;
          dataevents[5]=dataeventsvalue;
      }
  });
     
     
     
     
  var chart= document.getElementById("eventchart").getContext("2d"); 
  var datachart= new Chart(chart, {
      type:"bar", 
      data:{
          labels:[
              "Hackathons/Competitions",
              "Volunteering ",
              "Conferences / Forums ",
              "Workshops",
              "Green Festivals",
              "Official Holidays"

        ],
         datasets:[{
                 label:"Amount",
                 data:[ 
                     dataevents[0],
                     dataevents[1],
                     dataevents[2],
                     dataevents[3],
                     dataevents[4],
                     dataevents[5] 
                 ],
                 backgroundColor:[
                     "Green","Blue","Yellow","Red","Orange","Purple","Black","Pink"
                 ]
         }] 
      },
      options:{
          title:{
              display: true,
              text:"Events and challenges",
              fontSize:25
          },
          legend:{
              display:false
          }
      }
  });
});
 }else if(document.body.contains(document.getElementById('donationchart'))){
    // Your web app's Firebase configuration

var messageRef= firebase.database().ref("/Donations/Amount").once("value").then(function(snapshot){
    
        var chart= document.getElementById("donationchart").getContext("2d"); 
  var datachart= new Chart(chart, {
      type:"bar", 
      data:{
          labels:["Donations"          
        ],
         datasets:[{
                 label:"Amount",
                 data:[ 
                     snapshot.val()
                 ],
                 backgroundColor:[
                     "Green"
                 ]
         }] 
      },
      options:{
          title:{
              display: true,
              text:"Donations",
              fontSize:25
          },
          legend:{
              display:false
          }
      }
  });
   
});
     
 }else if(document.body.contains(document.getElementById('placeschart'))){
      var chart= document.getElementById("placeschart").getContext("2d"); 
  var datachart= new Chart(chart, {
      type:"bar", 
      data:{
          labels:[ "Hafencity",
              "Altona",
              "Elbphilarmonie",
              "Reeperbahn",
              "Jungfernstieg",
              "Miniature Land"
        ],
         datasets:[{
                 label:"Amount",
                 data:[ 
                     7,
                     5,
                     6,
                     8,
                     4,
                     7 
                 ],
                 backgroundColor:[
                     "Green","Blue","Yellow","Red","Orange","Purple","Black","Pink"
                 ]
         }] 
      },
      options:{
          title:{
              display: true,
              text:"Geodata (places)",
              fontSize:25
          },
          legend:{
              display:false
          }
      }
  });
     
 }else if(document.body.contains(document.getElementById('productschart'))){
     
     

  var dataevents=[0,0,0,0,0,0];
  //0: hackathon, 1:volunteer ,2: conference,3: workshop,4: festival,5:holiday
  // Initialize Firebase

var messageRef= firebase.database().ref("/Cities/Hamburg/Products").once("value", function(snapshot) {
  snapshot.forEach(function(childSnapshot) {
      if(childSnapshot.child("category").val()=="food"){
          var dataeventsvalue=dataevents[0]+1;
          dataevents[0]=dataeventsvalue;            
      }else if(childSnapshot.child("category").val()=="supplements"){
            var dataeventsvalue=dataevents[1]+1;
          dataevents[1]=dataeventsvalue;
      }else if(childSnapshot.child("category").val()=="healthcare "){
            var dataeventsvalue=dataevents[2]+1;
          dataevents[2]=dataeventsvalue;
      }else if(childSnapshot.child("category").val()=="fashionandclothing"){
            var dataeventsvalue=dataevents[3]+1;
          dataevents[3]=dataeventsvalue;
      }else if(childSnapshot.child("category").val()=="accessories "){
            var dataeventsvalue=dataevents[4]+1;
          dataevents[4]=dataeventsvalue;
      }else if(childSnapshot.child("category").val()=="specials"){
           var dataeventsvalue=dataevents[5]+1;
          dataevents[5]=dataeventsvalue;
      }
  });
     
     
     
     
  var chart= document.getElementById("productschart").getContext("2d"); 
  var datachart= new Chart(chart, {
      type:"pie", 
      data:{
          labels:["Food ",
                   "Supplements",
                   "Healthcare/beauty",
                   "Fashion/clothing",
                   "Accessories",
                   "Specials"
        ],
         datasets:[{
                 label:"Amount",
                 data:[ 
                    dataevents[0],
                     dataevents[1],
                     dataevents[2],
                     dataevents[3],
                     dataevents[4],
                     dataevents[5] 
                 ],
                 backgroundColor:[
                     "Green","Blue","Yellow","Red","Orange","Purple","Black","Pink"
                 ]
         }] 
      },
      options:{
          title:{
              display: true,
              text:"Type Products",
              fontSize:25
          },
          legend:{
              display:true
          }
      }
  });
  });   
 }else if(document.body.contains(document.getElementById('walkingchart'))){
     

  var datawalking=[0];
  // Initialize Firebase
 
var messageRef= firebase.database().ref("/Cities/Hamburg/walkingbikeurbees").once("value", function(snapshot) {
  snapshot.forEach(function(child) {
      datawalking.push(child.val());
  });
  
 var chart= document.getElementById("walkingchart").getContext("2d"); 
  var datachart= new Chart(chart, {
      type:"pie", 
      data:{
          labels:["Bike",
                   "Walking"           
        ],
         datasets:[{
                 label:"Time",
                 data:[ 
                     datawalking[1],
                     datawalking[2]

                 ],
                 backgroundColor:[
                     "Green","Blue","Yellow","Red","Orange","Purple","Black","Pink"
                 ]
         }] 
      },
      options:{
          title:{
              display: true,
              text:"Time walking/ riding a bike",
              fontSize:25
          },
          legend:{
              display:true
          }
      }
  }); 
  });

}else if (document.body.contains(document.getElementById('datamanagement'))){
    document.getElementById("useremailbutton").addEventListener("click", useremail);
    document.getElementById("usersearchbutton").addEventListener("click", usersearch);
    document.getElementById("eventssearchbutton").addEventListener("click", eventsearch);
    document.getElementById("productsearchbutton").addEventListener("click", productsearch);
    document.getElementById("placeseachbutton").addEventListener("click", placeseach);
    document.getElementById("lessonssearchbutton").addEventListener("click", lessonsearch);
    document.getElementById("eventaddbutton").addEventListener("click", eventadd);
    document.getElementById("productaddbutton").addEventListener("click", productadd);
    document.getElementById("placesaddbutton").addEventListener("click", placesadd);
    document.getElementById("lessonsaddbutton").addEventListener("click", lessonsadd);
    document.getElementById("generatecodes").addEventListener("click", generateCodes);
    document.getElementById("Check10codes").addEventListener("click", Check10codes);
    
}
//TODO solve  time issue
$("input.date").on("change", function() {
    this.setAttribute(
        "data-date",
        moment(this.value, "DD/MM/YYYY")
        .format( this.getAttribute("data-date-format") )
    )
}).trigger("change");
 });
 
     function useremail(){
  var email=  document.getElementById("datatext").value;
  let i= 0;
   firebase.database().ref("/Users").once("value", function(snapshot) {
  snapshot.forEach(function(children) {
    if(children.child("useremail").val()==email){   
          $("#datashown").html("");
          document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">User<p>'; 
          document.getElementById("datashown").innerHTML +='<input type="hidden" id="useridforedit'+i+'" name="custId" value="'+children.key+'">';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p  id="boldtitle">'+'ID: '+children.key+'</p></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="pemail'+i+'">'+'Email: '+children.child("useremail").val()+'</p><input class="searchbuttondata datasearch editdata " id="editemaildata'+i+'" type="email" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttoneditemaildata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="pname'+i+'">'+'Name: '+children.child("name").val()+'</p><input class="searchbuttondata datasearch editdata " id="editnamedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton buttoneditnamedata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="purbees'+i+'">'+'Urbees: '+children.child("urbees").val()+'</p><input class="searchbuttondata datasearch editdata " id="editurbeesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttonediturbeesdata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="pevents'+i+'">'+'Event points: '+children.child("eventtimes").val()+'</p><input class="searchbuttondata datasearch editdata " id="editeventdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttonediteventdata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="pplaces'+i+'">'+'Places points: '+children.child("turisticttimes").val()+'</p><input class="searchbuttondata datasearch editdata " id="editplacesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttoneditplacesdata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="pwalking'+i+'">'+'Walking points: '+children.child("mobilitytimes").val()+'</p><input class="searchbuttondata datasearch editdata " id="editwalkingdata'+i+'" type="number" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton buttoneditwalkingdata" >edit </button></div>';
   i++;
      }
  });
  editbuttoneventsactivate();
}); 


    }
 function usersearch(){
  $("#datashown").html("");
  let i= 0;
  firebase.database().ref("/Users").once("value", function(snapshot) {
    snapshot.forEach(function(children) {
          document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">User'+i+'<p>'; 
          document.getElementById("datashown").innerHTML +='<input type="hidden" id="useridforedit'+i+'" name="custId" value="'+children.key+'">';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p  id="boldtitle">'+'ID: '+children.key+'</p></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="pemail'+i+'">'+'Email: '+children.child("useremail").val()+'</p><input class="searchbuttondata datasearch editdata " id="editemaildata'+i+'" type="email" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttoneditemaildata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="pname'+i+'">'+'Name: '+children.child("name").val()+'</p><input class="searchbuttondata datasearch editdata " id="editnamedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton buttoneditnamedata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="purbees'+i+'">'+'Urbees: '+children.child("urbees").val()+'</p><input class="searchbuttondata datasearch editdata " id="editurbeesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttonediturbeesdata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="pevents'+i+'">'+'Event points: '+children.child("eventtimes").val()+'</p><input class="searchbuttondata datasearch editdata " id="editeventdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttonediteventdata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="pplaces'+i+'">'+'Places points: '+children.child("turisticttimes").val()+'</p><input class="searchbuttondata datasearch editdata " id="editplacesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton buttoneditplacesdata" >edit </button></div>';
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="pwalking'+i+'">'+'Walking points: '+children.child("mobilitytimes").val()+'</p><input class="searchbuttondata datasearch editdata " id="editwalkingdata'+i+'" type="number" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton buttoneditwalkingdata" >edit </button></div>';
          i++;
       });
       editbuttoneventsactivate();
    });   
   }
   
 function lessonsearch(){
  $("#datashown").html("");
  let i= 0;
  firebase.database().ref("/appnews").once("value", function(snapshot) {
    snapshot.forEach(function(children) {
        document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Lesson '+i+'<p>'; 
        document.getElementById("datashown").innerHTML +='<input type="hidden" id="lessonidforedit'+i+'" name="custId" value="'+children.key+'">';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p  id="boldtitle">'+'ID: '+children.key+'</p></div>';

        document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="lname'+i+'">'+'Name: '+children.child("name").val()+'</p><input class="searchbuttondata datasearch editdata " id="leditnamedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditnamedata" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="ltext'+i+'">'+'Text: '+children.child("text").val()+'</p><textarea class="searchbuttondata datasearch editdata " id="ledittextdata'+i+'" name="data" ></textarea><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttonedittextdata" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="lquestion'+i+'">'+'Question: '+children.child("answers").child("question").val()+'</p><input class="searchbuttondata datasearch editdata " id="leditquestiondata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditquestiondata" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="lanswer1'+i+'">'+'Answer 1: '+children.child("answers").child("1").val()+'</p><input class="searchbuttondata datasearch editdata " id="leditanswer1data'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditanswer1data" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="lanswer2'+i+'">'+'Answer 2: '+children.child("answers").child("2").val()+'</p><input class="searchbuttondata datasearch editdata " id="leditanswer2data'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditanswer2data" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="lanswer3'+i+'">'+'Answer 3: '+children.child("answers").child("3").val()+'</p><input class="searchbuttondata datasearch editdata " id="leditanswer3data'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditanswer3data" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="lcorrectanswer'+i+'">'+'Correct Answer: '+children.child("answers").child("correct").val()+'</p><select  class="searchbuttondata datasearch editdata " id="leditcorrectanswerdata'+i+'"name="data" > <option value="1"> 1 </option> <option value="2"> 2 </option>  <option value="3"> 3 </option>  </select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditcorrectanswerdata" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="limage'+i+'">'+'Image address: '+children.child("image").val()+'</p><input class="searchbuttondata datasearch editdata " id="leditimagedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditimagedata" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="llink'+i+'">'+'Source Link: '+children.child("link").val()+'</p><input class="searchbuttondata datasearch editdata " id="leditlinkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditlinkdata" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="lurbees'+i+'">'+'Urbees: '+children.child("urbees").val()+'</p><input class="searchbuttondata datasearch editdata " id="lediturbeesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton lbuttonediturbeesdata" >edit </button></div>';
        document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="lexpired'+i+'">'+'Expired: '+children.child("expired").val()+'</p><select  class="searchbuttondata datasearch editdata " id="leditexpireddata'+i+'"name="data" > <option value="yes"> Yes </option><option value="no"> No </option></select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton lbuttoneditexpireddata" >edit </button></div>';   
           i++;
       });
       lessonseditbuttoneventsactivate();
    });  
    
   }
   
   
     function eventsearch(){
      var city=  document.getElementById("citytext").value;
  $("#datashown").html("");
  let i= 0;
   firebase.database().ref("/Cities/"+city+"/Events").once("value", function(snapshot) {
  snapshot.forEach(function(children) {
      document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Events<p>'; 
      document.getElementById("datashown").innerHTML +='<input type="hidden" id="eventidforedit'+i+'" name="custId" value="'+children.key+'">';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p  id="boldtitle">'+'ID: '+children.key+'</p></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="ename'+i+'">'+'Name: '+children.child("name").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditnamedata'+i+'" type="text" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton ebuttoneditnamedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="edescription'+i+'">'+'Description: '+children.child("description").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditdescriptiondata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditdescriptiondata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="eurbees'+i+'">'+'Urbees: '+children.child("urbees").val()+'</p><input class="searchbuttondata datasearch editdata " id="eediturbeesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton ebuttonediturbeesdata" >edit </button></div>'; 
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="enumberofusers'+i+'">'+'Number of Users: '+children.child("numberofusers").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditnumberofusersdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton ebuttoneditnumberofusersdata" >edit </button></div>';  
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="eaddress'+i+'">'+'Address: '+children.child('address ').val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditaddressdata'+i+'" type="text" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton ebuttoneditaddressdata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="edate'+i+'">'+'Date: '+children.child("date").val()+'</p>Day:<input class="searchbuttondata datasearch editdata date " id="eeditdatedata'+i+'1" type="number" min="1" max="31" name="data" > Month:<input class="searchbuttondata datasearch editdata date " id="eeditdatedata'+i+'2" type="number"  min="1" max="12" name="data" > Year:<input class="searchbuttondata datasearch editdata date " id="eeditdatedata'+i+'3" type="number" min="2000" max="3000" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditdatedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="elat'+i+'">'+'Latitude: '+children.child("lat").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditlatdata'+i+'" type="number" step="any" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditlatdata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="elon'+i+'">'+'Longitude: '+children.child("lon").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditlondata'+i+'" type="number" step="any" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditlondata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ereservation_from'+i+'">'+'Reservation from: '+children.child("reservation_from").val()+'</p> Day:<input class="searchbuttondata datasearch editdata date" id="eeditreservation_fromdata'+i+'1" type="number" min="1" max="31" name="data" >Month:<input class="searchbuttondata datasearch editdata date" id="eeditreservation_fromdata'+i+'2" type="number" min="1" max="12" name="data" >Year:<input class="searchbuttondata datasearch editdata date" id="eeditreservation_fromdata'+i+'3" type="number" min="2000" max="3000" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditreservation_fromdata" >edit </button></div>';    
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="eimage'+i+'">'+'Image address: '+children.child("image").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditimagedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditimagedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="elink'+i+'">'+'Info Link: '+children.child("link").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditlinkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditlinkdata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="etype'+i+'">'+'Type: '+children.child("type").val()+'</p><select  class="searchbuttondata datasearch editdata " id="eedittypedata'+i+'"name="data" > <option value="hackathon"> Hackathon </option> <option value="workshop"> Workshop </option>  <option value="volunteer"> Volunteer </option>  <option value="conference"> Conference </option><option value="festival"> Festival </option> <option value="holiday"> Holiday </option></select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttonedittypedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ebeginen'+i+'">'+'Time it starts: '+children.child("beginen").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditbeginendata'+i+'1" type="number" min="0" max="24" name="data" >:<input class="searchbuttondata datasearch editdata " id="eeditbeginendata'+i+'2" type="number" name="data"  min="0" max="59" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditbeginendata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="eend'+i+'">'+'Time it finishes: '+children.child("end").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditenddata'+i+'1" type="number" name="data" min="0" max="24" >:<input class="searchbuttondata datasearch editdata " id="eeditenddata'+i+'2" type="number" name="data" min="0" max="59" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditenddata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ecode'+i+'">'+'Code: '+children.child("code").val()+'</p><input class="searchbuttondata datasearch editdata " id="eeditcodedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditcodedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="eexpired'+i+'">'+'Expired: '+children.child("expired").val()+'</p><select  class="searchbuttondata datasearch editdata " id="eeditexpireddata'+i+'"name="data" > <option value="yes"> Yes </option><option value="no"> No </option></select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ebuttoneditexpireddata" >edit </button></div>';
      i++;     
  });
eventseditbuttoneventsactivate(city);
}); 

    } 
     function productsearch(){
   var city=  document.getElementById("citytext").value;
  $("#datashown").html("");
  let i= 0;
   firebase.database().ref("/Cities/"+city+"/Products").once("value", function(snapshot) {
  snapshot.forEach(function(children) {
     if(children.key!="users"){
      document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Products<p>'; 
      document.getElementById("datashown").innerHTML +='<input type="hidden" id="eventidforedit'+i+'" name="custId" value="'+children.key+'">';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p  id="boldtitle">'+'ID: '+children.key+'</p></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="ppname'+i+'">'+'Name: '+children.child("name").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditnamedata'+i+'" type="text" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton ppbuttoneditnamedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="ppdescription'+i+'">'+'Description: '+children.child("description").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditdescriptiondata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditdescriptiondata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="ppurbees'+i+'">'+'Urbees: '+children.child("urbees").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppediturbeesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton ppbuttonediturbeesdata" >edit </button></div>'; 
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppprovider'+i+'">'+'Provider: '+children.child('provider').val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditproviderdata'+i+'" type="text" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton ppbuttoneditproviderdata" >edit </button></div>';                                                                                                                                                                
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppvalid_until'+i+'">'+'Valid until: '+children.child("valid_until").val()+'</p>Day:<input class="searchbuttondata datasearch editdata date " id="ppeditvalid_untildata'+i+'1" type="number" min="1" max="31" name="data" > Month:<input class="searchbuttondata datasearch editdata date " id="ppeditvalid_untildata'+i+'2" type="number"  min="1" max="12" name="data" > Year:<input class="searchbuttondata datasearch editdata date " id="ppeditvalid_untildata'+i+'3" type="number" min="2000" max="3000" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditvalid_untildata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppimage'+i+'">'+'Image address: '+children.child("image").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditimagedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditimagedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="pptype'+i+'">'+'Type: '+children.child("type").val()+'</p><select  class="searchbuttondata datasearch editdata " id="ppedittypedata'+i+'"name="data" > <option value="send"> Send </option> <option value="claim"> Claim </option>  <option value="link"> Link </option>  <option value="download"> Download </option><option value="thirdparty"> Third Party </option> </select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttonedittypedata" >edit </button></div>';
     let type= children.child("type").val();
      if (type=="send"){
         var type_of_link = "Info link: "; 
      }else if(type=="link"){  
     document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppdownload_link'+i+'">'+'Purchase Link: </p><p id="ppdownload_link'+i+'2">'+children.child("download_link").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditdownload_linkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditdownload_linkdata" >edit </button></div>';    
      var type_of_link = "Info link: "; 
      }else if(type=="download"){
         document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppdownload_link'+i+'">'+'Download after purchase Link:</p><p id="ppdownload_link'+i+'2">'+children.child("download_link").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditdownload_linkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditdownload_linkdata" >edit </button></div>';    
      var type_of_link = "Info link: ";   
      }else if(type=="thirdparty"){
          var type_of_link = "Contact link: ";
      }else if(type=="claim"){
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppdownload_link'+i+'">'+'Code:</p><p id="ppdownload_link'+i+'2">'+children.child("download_link").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditdownload_linkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditdownload_linkdata" >edit </button></div>';    
          document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppaddress'+i+'">'+'Claim Address:</p><p id="ppaddress'+i+'2">'+children.child("address").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppaddressdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttonaddressdata" >edit </button></div>';    
        
           var type_of_link = "Info link: ";
      }else if(type=="raffle"){
          var type_of_link = "Discount link: ";
           document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppdownload_link'+i+'">'+'Winner link: </p><p id="ppdownload_link'+i+'2">'+children.child("download_link").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditdownload_linkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditdownload_linkdata" >edit </button></div>';    
         
      }
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="pplink'+i+'1">'+type_of_link+'</p><p id="pplink'+i+'2">'+children.child("link").val()+'</p><input class="searchbuttondata datasearch editdata " id="ppeditlinkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditlinkdata" >edit </button></div>';
     document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppcategory'+i+'">'+'Category: '+children.child("category").val()+'</p><select  class="searchbuttondata datasearch editdata " id="ppeditcategorydata'+i+'"name="data" > <option value="food"> Food </option> <option value="supplements"> Supplements </option>  <option value="accessories"> Accessories </option>  <option value="specials"> Specials </option><option value="fashionandclothing"> Fashion/Clothing </option> <option value="healthcare"> Healthcare/Beauty </option></select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditcategorydata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="ppexpired'+i+'">'+'Expired: '+children.child("expired").val()+'</p><select  class="searchbuttondata datasearch editdata " id="ppeditexpireddata'+i+'"name="data" > <option value="yes"> Yes </option><option value="no"> No </option></select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton ppbuttoneditexpireddata" >edit </button></div>'; 
      i++; 
  }
  });
productseditbuttoneventsactivate(city);
}); 
    
    }
     function placeseach(){
   var city=  document.getElementById("citytext").value;
  $("#datashown").html("");
  let i= 0;
   firebase.database().ref("/Cities/"+city+"/Tplaces").once("value", function(snapshot) {
  snapshot.forEach(function(children) {
      document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Places<p>'; 
      document.getElementById("datashown").innerHTML +='<input type="hidden" id="eventidforedit'+i+'" name="custId" value="'+children.key+'">';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p  id="boldtitle">'+'ID: '+children.key+'</p></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="tname'+i+'">'+'Name: '+children.child("name").val()+'</p><input class="searchbuttondata datasearch editdata " id="teditnamedata'+i+'" type="text" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton tbuttoneditnamedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="tdescription'+i+'">'+'Description: '+children.child("description").val()+'</p><input class="searchbuttondata datasearch editdata " id="teditdescriptiondata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton tbuttoneditdescriptiondata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay"  ><p id="turbees'+i+'">'+'Urbees: '+children.child("urbees").val()+'</p><input class="searchbuttondata datasearch editdata " id="tediturbeesdata'+i+'" type="number" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton tbuttonediturbeesdata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="taddress'+i+'">'+'Address: '+children.child('address').val()+'</p><input class="searchbuttondata datasearch editdata " id="teditaddressdata'+i+'" type="text" name="data" ><button value="'+i+'"   class="btn btn-default send_contanct searchedbutton tbuttoneditaddressdata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="tlat'+i+'">'+'Latitude: '+children.child("lat").val()+'</p><input class="searchbuttondata datasearch editdata " id="teditlatdata'+i+'" type="number" step="any" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton tbuttoneditlatdata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="tlon'+i+'">'+'Longitude: '+children.child("lon").val()+'</p><input class="searchbuttondata datasearch editdata " id="teditlondata'+i+'" type="number" step="any" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton tbuttoneditlondata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="timage'+i+'">'+'Image address: '+children.child("image").val()+'</p><input class="searchbuttondata datasearch editdata " id="teditimagedata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton tbuttoneditimagedata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="tlink'+i+'">'+'Info Link: '+children.child("link").val()+'</p><input class="searchbuttondata datasearch editdata " id="teditlinkdata'+i+'" type="text" name="data" ><button value="'+i+'"  class="btn btn-default send_contanct searchedbutton tbuttoneditlinkdata" >edit </button></div>';
      document.getElementById("datashown").innerHTML +='<div class="linedisplay" ><p id="tactive'+i+'">'+'Active: '+children.child("active").val()+'</p><select  class="searchbuttondata datasearch editdata " id="teditactivedata'+i+'"name="data" > <option value="yes"> Yes </option><option value="no"> No </option></select> <button value="'+i+'"  class="btn btn-default send_contanct searchedbutton tbuttoneditactivedata" >edit </button></div>';  
      i++;       
  });
placeseditbuttoneventsactivate(city);
}); 
  
    
    }
    
    
     function eventadd(){
          $("#datashown").html("");
          document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Event<p>'; 
          document.getElementById("datashown").innerHTML +=' <form id="createeventform"></form>';
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>City: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcityforcreate" type="text" name="data" >';
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Name: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatename" type="text" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Description: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatedescription" type="text" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Address: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreateaddress" type="text" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Image address: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreateimage" type="text" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreateurbees" type="number" name="data" >';
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Latitude: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatelatitude" type="number" step="any" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Longitude: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatelongitud" type="number" step="any" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Info link: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatelink" type="text" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Code:</p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatecode" type="text" name="data" >'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Time to start :</p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatetime1" type="number" min="0" max="24" name="data"> :  <input class="searchbuttondata datasearch editdata " id="Eventcreatetime2" type="number" min="0" max="59" name="data">'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Time to end :</p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatetimeend1" type="number" min="0" max="24" name="data"> :  <input class="searchbuttondata datasearch editdata " id="Eventcreatetimeend2" type="number" min="0" max="59" name="data">'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Type :</p></div> <select class="searchbuttondata datasearch editdata " id="Eventcreatetype" name="data"> <option value="hackathon"> Hackathon </option> <option value="workshop"> Workshop </option>  <option value="volunteer"> Volunteer </option>  <option value="conference"> Conference </option><option value="festival"> Festival </option> <option value="holiday"> Holiday </option></select>'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Nomber of users: </p></div> <input required class="searchbuttondata datasearch editdata " id="Eventcreatenumberofursers" type="number" name="data" >';                                                      
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Expired :</p></div> <select class="searchbuttondata datasearch editdata " id="Eventcreateexpired" name="data"> <option value="yes"> yes </option> <option value="no"> no </option>'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Date :</p></div> Day:<input required class="searchbuttondata datasearch editdata date " id="Eventcreatedate1" type="number" min="1" max="31" name="data"> Month:<input class="searchbuttondata datasearch editdata date " id="Eventcreatedate2" type="number" min="1" max="12" name="data"> Year:<input class="searchbuttondata datasearch editdata date " id="Eventcreatedate3" type="number" min="2000" max="3000" name="data">'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay" ><p>Reservation from Day :</p></div> Day:<input  requiredclass="searchbuttondata datasearch editdata date " id="Eventcreatereservationdate1" type="number" min="1" max="31" name="data"> Month:<input class="searchbuttondata datasearch editdata date " id="Eventcreatereservationdate2" type="number" min="1" max="12" name="data"> Year:<input class="searchbuttondata datasearch editdata date " id="Eventcreatereservationdate3" type="number" min="2000" max="3000" name="data">'; 
          document.getElementById("createeventform").innerHTML +='<div class="linedisplay"> <button class="btn btn-default send_contanct searchedbutton buttoncreateevent" type="submit" >Create </button></div>';
         
           $("#createeventform").on("submit",function(e){
          e.preventDefault();
           firebase.database().ref("/Cities/"+$("#Eventcityforcreate").val()+"/Events").push(
                   {
                       "Users": {
                           "user" : {"went":"no" }
                       },
                      "address " : $("#Eventcreateaddress").val(), 
                      "beginen": $("#Eventcreatetime1").val()+":"+$("#Eventcreatetime2").val(),  
                      "code" : $("#Eventcreatecode").val(),
                      "date":  $("#Eventcreatedate1").val()+"."+$("#Eventcreatedate2").val()+"."+$("#Eventcreatedate3").val(),  
                      "description": $("#Eventcreatedescription").val(),
                      "end": $("#Eventcreatetimeend1").val()+":"+$("#Eventcreatetimeend2").val(),
                      "expired" : $("#Eventcreateexpired").val(),
                      "image": $("#Eventcreateimage").val(),
                      "lat": Number($("#Eventcreatelatitude").val()),
                      "link" : $("#Eventcreatelink").val(),
                      "lon" : Number($("#Eventcreatelongitud").val()),
                      "name": $("#Eventcreatename").val(),
                      "numberofusers":  Number($("#Eventcreatenumberofursers").val()),
                      "reservation_from":  $("#Eventcreatereservationdate1").val()+"."+$("#Eventcreatereservationdate2").val()+"."+$("#Eventcreatereservationdate3").val(),  
                      "type": $("#Eventcreatetype").val(),
                      "urbees": Number($("#Eventcreateurbees").val())
                      }
                     ).then(function(){
               alert("Created");
               $("#createproductform").find("input").val("");
           });
           
       });
    
     
     
     }
     function productadd(){
         $("#datashown").html(""); 
         document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Product<p>'; 
          document.getElementById("datashown").innerHTML +=' <form id="createproductform"></form>';
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>City: </p></div> <input required class="searchbuttondata datasearch editdata " id="Productcityforcreate" type="text" name="data" >'; 
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Name: </p></div> <input required class="searchbuttondata datasearch editdata " id="Productcreatename" type="text" name="data" >'; 
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Description: </p></div> <input required class="searchbuttondata datasearch editdata " id="Productcreatedescription" type="text" name="data" >';
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Image address: </p></div> <input required class="searchbuttondata datasearch editdata " id="Productcreateimage" type="text" name="data" >'; 
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Amount: </p></div> <input required class="searchbuttondata datasearch editdata" id="Productcreateamount" type="number" name="data" >';
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Provider: </p></div> <input required class="searchbuttondata datasearch editdata " id="Productcreateprovider" type="text" name="data" >';   
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Expired :</p></div> <select class="searchbuttondata datasearch editdata " id="Productcreateexpired" name="data"> <option value="yes"> yes </option> <option value="no"> no </option>'; 
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Valid until :</p></div> Day:<input required class="searchbuttondata datasearch editdata date " id="Productcreatevaliduntildate1" type="number" min="1" max="31" name="data"> Month:<input class="searchbuttondata datasearch editdata date " id="Productcreatevaliduntildate2" type="number" min="1" max="12" name="data"> Year:<input class="searchbuttondata datasearch editdata date " id="Productcreatevaliduntildate3" type="number" min="2000" max="3000" name="data">';
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p >Category: </p></div><select  class="searchbuttondata datasearch editdata " id="Productcreatecategory" name="data" > <option value="food"> Food </option> <option value="supplements"> Supplements </option>  <option value="accessories"> Accessories </option>  <option value="specials"> Specials </option><option value="fashionandclothing"> Fashion/Clothing </option> <option value="healthcare"> Healthcare/Beauty </option></select>';
          document.getElementById("createproductform").innerHTML +='<div class="linedisplay" ><p>Type :</p></div><select class="searchbuttondata datasearch editdata " id="Productcreatetype" name="data"><option value=""> </option> <option value="send"> Send </option> <option value="claim"> Claim </option>  <option value="link"> Link </option>  <option value="download"> Download </option><option value="raffle"> Raffle </option><option value="thirdparty"> Third Party </option> </select>'; 
          document.getElementById("createproductform").innerHTML +='<div id="aftertypeproduct"></div>';
        
         $("#Productcreatetype").on("change", function(){
             $("#aftertypeproduct").html("");
             if($("#Productcreatetype").val()=="send"){
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata" id="Productcreateurbees" type="number" name="data" >';
                  
                 document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Info link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatelink" type="text" name="data" >'; 
                 document.getElementById("aftertypeproduct").innerHTML +='<input required class="searchbuttondata datasearch editdata " id="productcreatedownloadlink" type="hidden" name="data value="" >'; 
            
             }else if($("#Productcreatetype").val()=="claim"){
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata" id="Productcreateurbees" type="number" name="data" >';
         
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Info link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatelink" type="text" name="data" >'; 
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Code: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatedownloadlink" type="text" name="data" >'; 
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Address for claim: </p></div> <input required class="searchbuttondata datasearch editdata " id="Productcreateaddress" type="text" name="data" >'; 
         
         
             }else if($("#Productcreatetype").val()=="link"){
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata" id="Productcreateurbees" type="number" name="data" >';
         
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Info link: </p></div> <input  required class="searchbuttondata datasearch editdata " id="productcreatelink" type="text" name="data" >'; 
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Purchase link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatedownloadlink" type="text" name="data" >'; 
              
             
             }else if($("#Productcreatetype").val()=="download"){
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata" id="Productcreateurbees" type="number" name="data" >';
         
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Info link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatelink" type="text" name="data" >'; 
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Download after purchase link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatedownloadlink" type="text" name="data" >'; 
              
         
             }else if($("#Productcreatetype").val()=="raffle"){
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata" id="Productcreateurbees" type="number" name="data" >';
         
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Discount link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatelink" type="text" name="data" >'; 
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Winner link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatedownloadlink" type="text" name="data" >'; 
              
         
             }else if($("#Productcreatetype").val()=="thirdparty"){
                  document.getElementById("aftertypeproduct").innerHTML +=' <input required class="searchbuttondata datasearch editdata" id="Productcreateurbees" value="-1" type="hidden" name="data" >';
                  
                  document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay" ><p>Contact link: </p></div> <input required class="searchbuttondata datasearch editdata " id="productcreatelink" type="text" name="data" >'; 
                   document.getElementById("aftertypeproduct").innerHTML +='<input required class="searchbuttondata datasearch editdata " id="productcreatedownloadlink" type="hidden" name="data value="" >'; 
             }
             document.getElementById("aftertypeproduct").innerHTML +='<div class="linedisplay"> <button type="submit" class="btn btn-default send_contanct searchedbutton buttoncreateproduct" >Create </button></div>';
              if($("#Productcreatetype").val()==""){
                   $("#aftertypeproduct").html("");
              }
         });
         

      $("#createproductform").on("submit",function(e){
          e.preventDefault();
           firebase.database().ref("/Cities/"+$("#Productcityforcreate").val()+"/Products").push(
                   {
                      "address" : ($("#Productcreateaddress").length ? $("#Productcreateaddress").val():"none") , 
                      "amount" :  Number($("#Productcreateamount").val()),
                      "category" : $("#Productcreatecategory").val(),
                      "description": $("#Productcreatedescription").val(),
                      "download_link": $("#productcreatedownloadlink").val(),
                      "expired" : $("#Productcreateexpired").val(),
                      "image": $("#Productcreateimage").val(),
                      "link" : $("#productcreatelink").val(),
                      "name": $("#Productcreatename").val(),
                      "provider": $("#Productcreateprovider").val(),
                      "type": $("#Productcreatetype").val(),
                      "urbees": Number($("#Productcreateurbees").val()),
                      "users": {
                           "user": {
                           "bought" : "no",
                           "raffle" : 0,
                           "times" : 0
                       }
                       },
                       "valid_until":  $("#Productcreatevaliduntildate1").val()+"."+$("#Productcreatevaliduntildate2").val()+"."+$("#Productcreatevaliduntildate3").val()  
                    }
                     ).then(function(){
               
               alert("Created");
               $("#createproductform").find("input").val("");
           });
           
       });
    
    }
     function placesadd(){
          $("#datashown").html("");
          document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Place<p>'; 
           document.getElementById("datashown").innerHTML +=' <form id="createplaceform"></form>';
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>City: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescityforcreate" type="text" name="data" >';
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Name: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placestcreatename" type="text" name="data" >'; 
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Description: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescreatedescription" type="text" name="data" >'; 
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Address: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescreateaddress" type="text" name="data" >'; 
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Image address: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescreateimage" type="text" name="data" >'; 
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescreateurbees" type="number" name="data" >';
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Latitude: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescreatelatitude" type="number" step="any" name="data" >'; 
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Longitude: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescreatelongitud" type="number" step="any" name="data" >'; 
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Info link: </p></div> <input required class="searchbuttondata datasearch editdata " id="Placescreatelink" type="text" name="data" >'; 
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay" ><p>Active :</p></div> <select class="searchbuttondata datasearch editdata " id="Placescreateactive" name="data"> <option value="yes"> yes </option> <option value="no"> no </option>';
          document.getElementById("createplaceform").innerHTML +='<div class="linedisplay"> <button type="submit" class="btn btn-default send_contanct searchedbutton buttoncreateplace" >Create </button></div>';

         $("#createplaceform").on("submit",function(e){
          e.preventDefault();
           firebase.database().ref("/Cities/"+$("#Placescityforcreate").val()+"/Tplaces").push(
                   {
                       "Users": {
                           "user" : "no"
                       },
                      "active" : $("#Placescreateactive").val(),
                      "address" : $("#Placescreateaddress").val(), 
                      "description": $("#Placescreatedescription").val(),
                      "image": $("#Placescreateimage").val(),
                      "lat": Number($("#Placescreatelatitude").val()),
                      "link" : $("#Placescreatelink").val(),
                      "lon" : $("#Placescreatelongitud").val(),
                      "name": $("#Placestcreatename").val(),
                      "urbees": Number($("#Placescreateurbees").val())
                    }
                     ).then(function(){
               
               alert("Created");
               $("#createplaceform").find("input").val("");
           });
           
       });
    }
   function lessonsadd(){
          $("#datashown").html("");
          document.getElementById("datashown").innerHTML +='<p class="titleadminactivity">Lesson<p>'; 
          document.getElementById("datashown").innerHTML +=' <form id="createlessonform"></form>';
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Name: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonstcreatename" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Text: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreatetext" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Image address: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreateimage" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Source link: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreatelink" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Urbees: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreateurbees" type="number" name="data" >';
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Expired :</p></div> <select class="searchbuttondata datasearch editdata " id="Lessonscreateexpired" name="data"> <option value="yes"> yes </option> <option value="no"> no </option>';
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Question: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreatequestion" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Answer 1: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreateanswer1" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Answer 2: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreateanswer2" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Answer 3: </p></div> <input required class="searchbuttondata datasearch editdata " id="Lessonscreateanswer3" type="text" name="data" >'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay" ><p>Correct answer :</p></div> <select class="searchbuttondata datasearch editdata " id="Lessonscreatecorrectanswer" name="data"> <option value="1">1</option> <option value="2">2</option><option value="3">3</option>'; 
          document.getElementById("createlessonform").innerHTML +='<div class="linedisplay"> <button type="submit" class="btn btn-default send_contanct searchedbutton buttoncreatelessons" >Create </button></div>';

         $("#createlessonform").on("submit",function(e){
          e.preventDefault();
           firebase.database().ref("/appnews/").push(
                   {
                       "answers": {      
                         "1" : $("#Lessonscreateanswer1").val(),
                         "2" : $("#Lessonscreateanswer2").val(),
                         "3" :  $("#Lessonscreateanswer3").val(),
                         "correct" :Number($("#Lessonscreatecorrectanswer").val()),
                         "question" :  $("#Lessonscreatequestion").val()
                       },
                      "expired" : $("#Lessonscreateexpired").val(),
                      "image": $("#Lessonscreateimage").val(),
                      "link" : $("#Lessonscreatelink").val(),
                      "name": $("#Lessonstcreatename").val(),   
                      "text": $("#Lessonscreatetext").val(),
                      "urbees": Number($("#Lessonscreateurbees").val()),
                       "users": {
                           "userpilot" : {
                                "claimed" : "no",
                                "try" : 0
                            }
                       }
                    }
                     ).then(function(){
               
               alert("Created");
               $("#createplaceform").find("input").val("");
           });
           
       });
    }
    
    
 //User edit events
 function editbuttoneventsactivate(){

  $(".buttoneditemaildata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#editemaildata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#useridforedit"+id).val();
        firebase.database().ref("/Users/"+userid+"/useremail").set(datatochange).then(function(){
            $("#editemaildata"+id).val("");
            $("#pemail"+id).text("Email:"+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
   $(".buttoneditnamedata").on("click",function(){
       let id= $(this).val();
       let datatochange= $("#editnamedata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#useridforedit"+id).val();
        firebase.database().ref("/Users/"+userid+"/name").set(datatochange).then(function(){
            $("#editnamedata"+id).val("");
            alert("data changed");
            $("#pname"+id).text("Name:"+datatochange);
       }); 
   }
  });
  
 $(".buttonediturbeesdata").on("click",function(){
         let id= $(this).val();
         
          let datatochange= $("#editurbeesdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#useridforedit"+id).val();
        firebase.database().ref("/Users/"+userid+"/urbees").set(datatochange).then(function(){
            $("#editurbeesdata"+id).val("");
            alert("data changed");
            $("#purbees"+id).text("Urbees: "+datatochange);
       }); 
   }
      
  });
    $(".buttonediteventdata").on("click",function(){
        let id= $(this).val();
        
         let datatochange= $("#editeventdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#useridforedit"+id).val();
        firebase.database().ref("/Users/"+userid+"/eventtimes").set(datatochange).then(function(){
            $("#editeventdata"+id).val("");
            alert("data changed");
            $("#pevents"+id).text("Event points: "+datatochange);
       }); 
   }
        
      
  });
  $(".buttoneditwalkingdata").on("click",function(){
      let id= $(this).val();
      
     let datatochange= $("#editwalkingdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#useridforedit"+id).val();
        firebase.database().ref("/Users/"+userid+"/mobilitytimes").set(datatochange).then(function(){
            $("#editwalkingdata"+id).val("");
            alert("data changed");
            $("#pwalking"+id).text("Walking points: "+datatochange);
       }); 
   }  
      
      
  });
  $(".buttoneditplacesdata").on("click",function(){
      let id= $(this).val();
      
      let datatochange= $("#editplacesdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#useridforedit"+id).val();
        firebase.database().ref("/Users/"+userid+"/turisticttimes").set(datatochange).then(function(){
            $("#editplacesdata"+id).val("");
            alert("data changed");
            $("#pplaces"+id).text("Places points: "+datatochange);
       }); 
   }  
      
  });
}
//Events edit events
function eventseditbuttoneventsactivate(city){
    
     $(".ebuttoneditnamedata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditnamedata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/name").set(datatochange).then(function(){
            $("#eeditnamedata"+id).val("");
            $("#ename"+id).text("Name:"+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
    $(".ebuttoneditdescriptiondata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditdescriptiondata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/description").set(datatochange).then(function(){
            $("#eeditdescriptiondata"+id).val("");
            $("#edescription"+id).text("Description: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
    $(".ebuttonediturbeesdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eediturbeesdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/urbees").set(Number(datatochange)).then(function(){
            $("#eediturbeesdata"+id).val("");
            $("#eurbees"+id).text("Urbees: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
  
  $(".ebuttoneditnumberofusersdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditnumberofusersdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/numberofusers").set(Number(datatochange)).then(function(){
            $("#eeditnumberofusersdata"+id).val("");
            $("#enumberofusers"+id).text("Number of users: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
   $(".ebuttoneditaddressdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditaddressdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/address ").set(datatochange).then(function(){
            $("#eeditaddressdata"+id).val("");
            $("#eaddress"+id).text("Address: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
  
   $(".ebuttoneditdatedata").on("click",function(){
      let id= $(this).val();
   let datatochange1= $("#eeditdatedata"+id+"1").val();
    let datatochange2= $("#eeditdatedata"+id+"2").val();
     let datatochange3= $("#eeditdatedata"+id+"3").val();
     let datatochangetotal= datatochange1+"."+datatochange2+"."+datatochange3;
   if(datatochange1 !=""&& datatochange2 !="" && datatochange3 !=""){
       if(!((datatochange1 > 31 || datatochange1 < 1) || (datatochange2 > 12 || datatochange2 < 1) || (datatochange3 > 3000 || datatochange3 < 2019))){
        let userid= $("#eventidforedit"+id).val();
         firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/date").set(datatochangetotal).then(function(){
             $("#eeditdatedata"+id+"1").val("");
             $("#eeditdatedata"+id+"2").val("");
             $("#eeditdatedata"+id+"3").val("");
             $("#edate"+id).text("Date: "+datatochangetotal);
             alert("data changed");
        }); 
    }else{
        alert("The date is incorrect");
    }
       
   }
   
      
  });
  
   $(".ebuttoneditlatdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditlatdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/lat").set(Number(datatochange)).then(function(){
            $("#eeditlatdata"+id).val("");
            $("#elat"+id).text("Latitude: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
    $(".ebuttoneditlondata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditlondata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/lon").set(Number(datatochange)).then(function(){
            $("#eeditlondata"+id).val("");
            $("#elon"+id).text("Longitude: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
    $(".ebuttoneditreservation_fromdata").on("click",function(){
      let id= $(this).val();
   let datatochange1= $("#eeditreservation_fromdata"+id+"1").val();
   let datatochange2= $("#eeditreservation_fromdata"+id+"2").val();
   let datatochange3= $("#eeditreservation_fromdata"+id+"3").val();
   let datatochangetotal= datatochange1+"."+datatochange2+"."+datatochange3;
   if(datatochange1 !=""&& datatochange2 !="" && datatochange3 !=""){
       if(!((datatochange1 > 31 || datatochange1 < 1) || (datatochange2 > 12 || datatochange2 < 1) || (datatochange3 > 3000 || datatochange3 < 2019))){
            let userid= $("#eventidforedit"+id).val();
             firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/reservation_from").set(datatochangetotal).then(function(){
                 $("#eeditreservation_fromdata"+id+"1").val("");
                 $("#eeditreservation_fromdata"+id+"2").val("");
                 $("#eeditreservation_fromdata"+id+"3").val("");
                 $("#ereservation_from"+id).text("Reservation from: "+datatochange);
                 alert("data changed");
            }); 
        }else{
            alert("the date is incorrect");
        }
    }
    
  });
  
   $(".ebuttoneditimagedata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditimagedata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/image").set(datatochange).then(function(){
            $("#eeditimagedata"+id).val("");
            $("#eimage"+id).text("Image address: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
   $(".ebuttoneditlinkdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditlinkdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/link").set(datatochange).then(function(){
            $("#eeditlinkdata"+id).val("");
            $("#elink"+id).text("Link: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
  $(".ebuttonedittypedata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eedittypedata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/type").set(datatochange).then(function(){
            $("#eedittypedata"+id).val("");
            $("#etype"+id).text("Type : "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
   $(".ebuttoneditbeginendata").on("click",function(){
      let id= $(this).val();
   let datatochange1= $("#eeditbeginendata"+id+"1").val();
   let datatochange2= $("#eeditbeginendata"+id+"2").val();
   let datatochangetotal=datatochange1+":"+datatochange2;
   if(datatochange1 !="" && datatochange2!=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/beginen").set(datatochangetotal).then(function(){
            $("#eeditbeginendata"+id+"1").val("");
            $("#eeditbeginendata"+id+"2").val("");
            $("#ebeginen"+id).text("Time it starts: "+datatochangetotal);
            alert("data changed");
       }); 
   }
      
  });
    
    
    $(".ebuttoneditenddata").on("click",function(){
      let id= $(this).val();
   let datatochange1= $("#eeditenddata"+id+"1").val();
   let datatochange2= $("#eeditenddata"+id+"2").val();
   let datatochangetotal=datatochange1+":"+datatochange2;
   if(datatochange1 !="" && datatochange2 !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/end").set(datatochangetotal).then(function(){
            $("#eeditenddata"+id+"1").val("");
            $("#eeditenddata"+id+"2").val("");
            $("#eend"+id).text("Time it finishes: "+datatochangetotal);
            alert("data changed");
       }); 
   }
      
  });
  
  $(".ebuttoneditcodedata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditcodedata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/code").set(datatochange).then(function(){
            $("#eeditcodedata"+id).val("");
            $("#ecode"+id).text("Code: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
   $(".ebuttoneditexpireddata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#eeditexpireddata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Events/"+userid+"/expired").set(datatochange).then(function(){
            $("#eeditexpireddata"+id).val("");
            $("#eexpired"+id).text("Expired: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
}
//Places edit events
function placeseditbuttoneventsactivate(city){
 
  $(".tbuttoneditnamedata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditnamedata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/name").set(datatochange).then(function(){
            $("#teditnamedata"+id).val("");
            $("#tname"+id).text("Name:"+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
  $(".tbuttoneditdescriptiondata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditdescriptiondata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/description").set(datatochange).then(function(){
            $("#teditdescriptiondata"+id).val("");
            $("#tdescription"+id).text("Description: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
    $(".tbuttonediturbeesdata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#tediturbeesdata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/urbees").set(Number(datatochange)).then(function(){
            $("#tediturbeesdata"+id).val("");
            $("#turbees"+id).text("Urbees: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
   $(".tbuttoneditaddressdata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditaddressdata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/address").set(datatochange).then(function(){
            $("#teditaddressdata"+id).val("");
            $("#taddress"+id).text("Address: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  $(".tbuttoneditlatdata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditlatdata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/lat").set(Number(datatochange)).then(function(){
            $("#teditlatdata"+id).val("");
            $("#tlat"+id).text("Latitude: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
   $(".tbuttoneditlondata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditlondata"+id).val();
   if(datatochange !=""){
       let datatochangevalue=Number(datatochange);
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/lon").set(datatochangevalue).then(function(){
            $("#teditlondata"+id).val("");
            $("#tlon"+id).text("Longitude: "+datatochange);
            alert("data changed");
       }); 
   }   
  });

  $(".tbuttoneditimagedata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditimagedata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/image").set(datatochange).then(function(){
            $("#teditimagedata"+id).val("");
            $("#timage"+id).text("Image address: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
   $(".tbuttoneditlinkdata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditlinkdata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/link").set(datatochange).then(function(){
            $("#teditlinkdata"+id).val("");
            $("#tlink"+id).text("Image address: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  $(".tbuttoneditactivedata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#teditactivedata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Tplaces/"+userid+"/active").set(datatochange).then(function(){
            $("#teditactivedata"+id).val("");
            $("#tactive"+id).text("Image address: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
}
//Products edit events
 function productseditbuttoneventsactivate(city){
     
     
       $(".ppbuttoneditnamedata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#ppeditnamedata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/name").set(datatochange).then(function(){
            $("#ppeditnamedata"+id).val("");
            $("#ppname"+id).text("Name:"+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
  $(".ppbuttoneditdescriptiondata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#ppeditdescriptiondata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/description").set(datatochange).then(function(){
            $("#ppeditdescriptiondata"+id).val("");
            $("#ppdescription"+id).text("Description: "+datatochange);
            alert("data changed");
       }); 
   }
      
  });
  
   $(".ppbuttonediturbeesdata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#ppediturbeesdata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/urbees").set(Number(datatochange)).then(function(){
            $("#ppediturbeesdata"+id).val("");
            $("#ppurbees"+id).text("Urbees: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  
    $(".ppbuttoneditproviderdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#ppeditproviderdata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/provider").set(datatochange).then(function(){
            $("#ppeditproviderdata"+id).val("");
            $("#ppprovider"+id).text("Provider: "+datatochange);
            alert("data changed");
       }); 
   }   
  });  
  
  $(".ppbuttoneditimagedata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#ppeditimagedata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/image").set(datatochange).then(function(){
            $("#ppeditimagedata"+id).val("");
            $("#ppimage"+id).text("Image address: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  
     $(".ppbuttoneditvalid_untildata").on("click",function(){
      let id= $(this).val();
   let datatochange1= $("#ppeditvalid_untildata"+id+"1").val();
    let datatochange2= $("#ppeditvalid_untildata"+id+"2").val();
     let datatochange3= $("#ppeditvalid_untildata"+id+"3").val();
     let datatochangetotal= datatochange1+"."+datatochange2+"."+datatochange3;
   if(datatochange1 !=""&& datatochange2 !="" && datatochange3 !=""){
       if(!((datatochange1 > 31 || datatochange1 < 1) || (datatochange2 > 12 || datatochange2 < 1) || (datatochange3 > 3000 || datatochange3 < 2019))){
            let userid= $("#eventidforedit"+id).val();
             firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/valid_until").set(datatochangetotal).then(function(){
                 $("#ppeditvalid_untildata"+id+"1").val("");
                 $("#ppeditvalid_untildata"+id+"2").val("");
                 $("#ppeditvalid_untildata"+id+"3").val("");
                 $("#ppvalid_until"+id).text("Valid until: "+datatochangetotal);
                 alert("data changed");
            }); 
      }else{
           alert("The date is wrong");
      }
   }  
  });
  
    $(".ppbuttoneditcategorydata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#ppeditcategorydata"+id).val();
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/category").set(datatochange).then(function(){
            $("#ppeditcategorydata"+id).val("");
            $("#ppcategory"+id).text("Category: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  
   $(".ppbuttoneditexpireddata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#ppeditexpireddata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/expired").set(datatochange).then(function(){
            $("#ppeditexpireddata"+id).val("");
            $("#ppexpired"+id).text("Expired: "+datatochange);
            alert("data changed");
       }); 
   }  
  });
  
  $(".ppbuttonedittypedata").on("click",function(){
     let id= $(this).val();
   let datatochange= $("#ppedittypedata"+id).val();
   
   if(datatochange !=""){
       let userid= $("#eventidforedit"+id).val();
        firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/type").set(datatochange).then(function(){
            $("#ppedittypedata"+id).val("");
            $("#pptype"+id).text("Type: "+datatochange);
            alert("data changed");
            $("#productsearchbutton").trigger("click");
       }); 
   }  
     });
     
   $(".ppbuttoneditlinkdata").on("click",function(){
    let id= $(this).val();
    let datatochange= $("#ppeditlinkdata"+id).val();
    if(datatochange !=""){
        let userid= $("#eventidforedit"+id).val();
         firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/link").set(datatochange).then(function(){
             $("#ppeditlinkdata"+id).val("");
             $("#pplink"+id+"2").text(datatochange);
             alert("data changed"); 
        }); 
    }  
     });
   if($(".ppbuttoneditdownload_linkdata")!= undefined){  
        $(".ppbuttoneditdownload_linkdata").on("click",function(){
            let id= $(this).val();
            let datatochange= $("#ppeditdownload_linkdata"+id).val();
            if(datatochange !=""){
                let userid= $("#eventidforedit"+id).val();
                 firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/download_link").set(datatochange).then(function(){
                     $("#ppeditdownload_linkdata"+id).val("");
                     $("#ppdownload_link"+id+"2").text(datatochange);
                     alert("data changed"); 
                }); 
            }  
     });
 }
   if($(".ppbuttonaddressdata")!= undefined){  //claimaddress
        $(".ppbuttonaddressdata").on("click",function(){
            let id= $(this).val();
            let datatochange= $("#ppaddressdata"+id).val();
            if(datatochange !=""){
                let userid= $("#eventidforedit"+id).val();
                 firebase.database().ref("/Cities/"+city+"/Products/"+userid+"/address").set(datatochange).then(function(){
                     $("#ppaddressdata"+id).val("");
                     $("#ppaddress"+id+"2").text(datatochange);
                     alert("data changed"); 
                }); 
            }  
     });
 }
 }
 
 function lessonseditbuttoneventsactivate(){

  $(".lbuttoneditnamedata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditnamedata"+id).val();
   
   if(datatochange !=""){
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/name").set(datatochange).then(function(){
            $("#leditnamedata"+id).val("");
            $("#lname"+id).text("Name: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  
   $(".lbuttonedittextdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#ledittextdata"+id).val();
   
   if(datatochange !=""){
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/text").set(datatochange).then(function(){
            $("#ledittextdata"+id).val("");
            $("#ltext"+id).text("Text: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
     $(".lbuttoneditquestiondata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditquestiondata"+id).val();
   
   if(datatochange !=""){
       //this value doesn't change
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/answers/question").set(datatochange).then(function(){
            $("#leditquestiondata"+id).val("");
            $("#lquestion"+id).text("Question: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  
     $(".lbuttoneditanswer1data").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditanswer1data"+id).val();
   
   if(datatochange !=""){
       //this value doesn't change
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/answers/1").set(datatochange).then(function(){
            $("#leditanswer1data"+id).val("");
            $("#lanswer1"+id).text("Answer1: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
    $(".lbuttoneditanswer2data").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditanswer2data"+id).val();
   
   if(datatochange !=""){
       //this value doesn't change
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/answers/2").set(datatochange).then(function(){
            $("#leditanswer2data"+id).val("");
            $("#lanswer2"+id).text("Answer2: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
    $(".lbuttoneditanswer3data").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditanswer3data"+id).val();
   
   if(datatochange !=""){
       //this value doesn't change
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/answers/3").set(datatochange).then(function(){
            $("#leditanswer3data"+id).val("");
            $("#lanswer3"+id).text("Answer3: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
   $(".lbuttoneditcorrectanswerdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditcorrectanswerdata"+id).val();
   
   if(datatochange !=""){
       //this value doesn't change
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/answers/correct").set(datatochange).then(function(){
            $("#leditcorrectanswerdata"+id).val("");
            $("#lcorrectanswer"+id).text("Correct Answer: "+datatochange);
            alert("data changed");
       }); 
   }   
  });  
    $(".lbuttoneditimagedata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditimagedata"+id).val();
   
   if(datatochange !=""){
       //this value doesn't change
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/image").set(datatochange).then(function(){
            $("#leditimagedata"+id).val("");
            $("#limage"+id).text("Image Address: "+datatochange);
            alert("data changed");
       }); 
   }   
  }); 
   $(".lbuttoneditlinkdata").on("click",function(){
      let id= $(this).val();
   let datatochange= $("#leditlinkdata"+id).val();
   
   if(datatochange !=""){
       //this value doesn't change
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/link").set(datatochange).then(function(){
            $("#leditlinkdata"+id).val("");
            $("#llink"+id).text("Image Address: "+datatochange);
            alert("data changed");
       }); 
   }   
  }); 
  
   $(".lbuttonediturbeesdata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#lediturbeesdata"+id).val();
   if(datatochange !=""){
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/urbees").set(Number(datatochange)).then(function(){
            $("#lediturbeesdata"+id).val("");
            $("#lurbees"+id).text("Urbees: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  
  $(".lbuttoneditexpireddata").on("click",function(){
   let id= $(this).val();
   let datatochange= $("#leditexpireddata"+id).val();
   if(datatochange !=""){
       let lessonid= $("#lessonidforedit"+id).val();
        firebase.database().ref("/appnews/"+lessonid+"/expired").set(datatochange).then(function(){
            $("#leditexpireddata"+id).val("");
            $("#lexpired"+id).text("Expired: "+datatochange);
            alert("data changed");
       }); 
   }   
  });
  
  
 }
 
 function Emails(){
  $("#datashown").html("");
  let i= 0;
   firebase.database().ref("/newsletter").once("value", function(snapshot) {
  snapshot.forEach(function(children) {
    document.getElementById("datashown").innerHTML +=' '+children.child("email").val()+', ';
      i++;       
  });
}); 
 }
 
 function generateCodes(){
     for(var i= 0; i<100; i++){
        firebase.database().ref("/codes").push(
                {
                    claimed: "no"
                } ); 
            }  
     alert("100 codes generated");
 }
function Check10codes(){
 $("#datashown").html("");
  document.getElementById("datashown").innerHTML +='<strong>10 available codes:</strong> <br/> <br/>';
 firebase.database().ref("/codes").orderByChild("claimed").equalTo('no').limitToFirst(10).once('value', function (snapshot) {
  snapshot.forEach(function(children) {
    document.getElementById("datashown").innerHTML +=' '+children.key+'<br/> ';

  });
  
}); 
    
}

